
import corba.EchoServicePOA;
import java.net.InetAddress;
import java.net.UnknownHostException;
import org.omg.CORBA.ORB;


public class EchoImpl extends EchoServicePOA{
    
      private ORB orb;
      String myURL="localhost";
      
    
    public void setORB(ORB orb_val){
        orb = orb_val;
    
    }

    @Override
    public String echo(String x) {
        try {
          myURL=InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
          myURL="localhost";
     }
        return " Eco del servidor " + myURL +" : ==> es " + x;
    }
    
}
